//
//  PangoViewer.hpp
//  vins
//
//  Created by 谭智丹 on 17/6/7.
//
//

#ifndef PangoViewer_hpp
#define PangoViewer_hpp

#include <Eigen/Dense>
#include <pangolin/pangolin.h>

class PangoViewer
{
public:
    
    PangoViewer();
    
    void DrawGrids(Eigen::Vector3d &origin, float grid_size);
    
    void DrawPoints(const std::vector<Eigen::Vector3d> &points, int size, const Eigen::Vector3f &color);
    
    void DrawIMU(Eigen::Quaterniond &q_I_G, Eigen::Vector3d &p_G_I, double scale);
    
    void DrawTrajectory(const std::vector<Eigen::Vector3d> &traj, const Eigen::Vector3f &color);
    
};

#endif /* PangoViewer_hpp */
