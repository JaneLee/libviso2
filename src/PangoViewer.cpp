//
//  PangoViewer.cpp
//  vins
//
//  Created by 谭智丹 on 17/6/7.
//
//

#include "PangoViewer.hpp"

using namespace std;
using namespace Eigen;

PangoViewer::PangoViewer() {}


void PangoViewer::DrawGrids(Eigen::Vector3d &origin, float grid_size)
{
    const float l5 = grid_size * 5;
    const float minx = origin(0) - l5;
    const float maxx = origin(0) + l5;
    const float miny = origin(1) - l5;
    const float maxy = origin(1) + l5;
    
    glLineWidth(2);
    glColor3f(0.2, 0.2, 0.2);
    glBegin(GL_LINES);
    
    float ln;
    for (int n=0; n<11; n++) {
        ln = grid_size * n;
        glVertex3f(minx+ln, miny, 0);
        glVertex3f(minx+ln, maxy, 0);
        glVertex3f(minx, miny+ln, 0);
        glVertex3f(maxx, miny+ln, 0);
    }
    
    glEnd();
}



void PangoViewer::DrawPoints(const vector<Vector3d> &points, int size, const Vector3f &color)
{
    glPointSize(size);
    glBegin(GL_POINT);
    glColor3f(color(0), color(1), color(2));
    
    size_t N = points.size();
    for (size_t i=0; i<N; i++)
        pangolin::glVertex(points[i]);
    
    glEnd();
}



void PangoViewer::DrawIMU(Quaterniond &q_I_G, Vector3d &p_G_I, double scale)
{
    Matrix3d sR_G_C = scale * q_I_G.matrix();
    
    glLineWidth(4);
    glBegin(GL_LINES);
    
    glColor3f(1.0, 0.0, 0.0);
    pangolin::glVertex(p_G_I);
    pangolin::glVertex(p_G_I+sR_G_C.col(0));
    
    glColor3f(0.0, 1.0, 0.0);
    pangolin::glVertex(p_G_I);
    pangolin::glVertex(p_G_I+sR_G_C.col(1));
    
    glColor3f(0.0, 0.0, 1.0);
    pangolin::glVertex(p_G_I);
    pangolin::glVertex(p_G_I+sR_G_C.col(2));
    
    glEnd();
}



void PangoViewer::DrawTrajectory(const std::vector<Eigen::Vector3d> &traj, const Vector3f &color)
{
    if (traj.size() < 2)
        return;
    
    glLineWidth(3);
    glColor3f(color(0), color(1), color(2));
    glBegin(GL_LINES);
    
    size_t N = traj.size() - 1;
    for (size_t i=0; i<N; i++) {
        pangolin::glVertex(traj[i]);
        pangolin::glVertex(traj[i+1]);
    }
    
    glEnd();
}