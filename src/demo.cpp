/*
Copyright 2012. All rights reserved.
Institute of Measurement and Control Systems
Karlsruhe Institute of Technology, Germany

This file is part of libviso2.
Authors: Andreas Geiger

libviso2 is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or any later version.

libviso2 is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
libviso2; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301, USA 
*/

/*
  Documented C++ sample code of stereo visual odometry (modify to your needs)
  To run this demonstration, download the Karlsruhe dataset sequence
  '2010_03_09_drive_0019' from: www.cvlibs.net!
  Usage: ./viso2 path/to/sequence/2010_03_09_drive_0019
*/

#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>

#include <viso_stereo.h>
#include "PangoViewer.hpp"
#include <opencv2/opencv.hpp>

using namespace std;

int main (int argc, char** argv) {

    // we need the path name to 2010_03_09_drive_0019 as input argument
    if (argc<2) {
        cerr << "Usage: ./viso2 path/to/sequence/2010_03_09_drive_0019" << endl;
        return 1;
    }

    /*------------------Pangolin----------------*/
    pangolin::CreateWindowAndBind("LIBVISO", 1024, 768);
    glEnable(GL_DEPTH_TEST);

    pangolin::OpenGlRenderState s_cam( pangolin::ProjectionMatrix(1024,768,500,500,512,389,0.1,1000),
            pangolin::ModelViewLookAt(0,0,150, 0,0,0,0.0,1, 0.0));
    pangolin::Handler3D handler(s_cam);
    pangolin::View& d_cam = pangolin::CreateDisplay()
    .SetBounds(0.0, 1.0, 0.0, 1.0)
    .SetHandler(&handler);

    PangoViewer pango;
    /*------------------------------------------*/


    // sequence directory
    string dir = argv[1];

    // set most important visual odometry parameters
    // for a full parameter list, look at: viso_stereo.h
    VisualOdometryStereo::parameters param;

    // calibration parameters for sequence 2010_03_09_drive_0019
    param.calib.f  = 645.24; // focal length in pixels
    param.calib.cu = 635.96; // principal point (u-coordinate) in pixels
    param.calib.cv = 194.13; // principal point (v-coordinate) in pixels
    param.base     = 0.5707; // baseline in meters

    // init visual odometry
    VisualOdometryStereo viso(param);

    // current pose (this matrix transforms a point from the current
    // frame's camera coordinates to the first frame's camera coordinates)
    Matrix pose = Matrix::eye(4);

    cv::Mat prev_left_img, prev_right_img;
    // load left and right input image
    cv::Mat left_img, right_img;
    
    // store R_G_C0 and p_G_C0
    Eigen::Vector3d p_G_C0 = Eigen::Vector3d::Zero();
    Eigen::Matrix3d R_G_C0;

    std::vector<Eigen::Vector3d> trajectory;

    // loop through all frames i=0:372
    for (int32_t i=0; i<112; i++) {

        // input file names
        char base_name[256]; sprintf(base_name,"%06d.png",i);
        string left_img_file_name  = dir + "/I1_" + base_name;
        string right_img_file_name = dir + "/I2_" + base_name;

        // catch image read/write errors here
        try {
            // store the current and previous stereo image pairs
            prev_left_img = left_img;
            prev_right_img = right_img;

            left_img = cv::imread(left_img_file_name, -1);
            right_img = cv::imread(right_img_file_name, -1);

            // image dimensions
            int32_t width  = left_img.cols;
            int32_t height = left_img.rows;
            int32_t half_width = width / 2;
            int32_t half_height = height / 2;

            // convert input images to uint8_t buffer
            uint8_t* left_img_data  = (uint8_t*)malloc(width*height*sizeof(uint8_t));
            uint8_t* right_img_data = (uint8_t*)malloc(width*height*sizeof(uint8_t));
            int32_t k=0;
            for (int32_t v=0; v<height; v++) {
                for (int32_t u=0; u<width; u++) {
                    left_img_data[k]  = left_img.at<uchar>(v, u);
                    right_img_data[k] = right_img.at<uchar>(v, u);
                    k++;
                }
            }

            // status
            cout << "Processing: Frame: " << i << " " ;

            // compute visual odometry
            int32_t dims[] = {width, height, width};
            if (viso.process(left_img_data, right_img_data, dims)) {

                // on success, update current pose
                pose = pose * Matrix::inv(viso.getMotion());

                // output some statistics
                double num_matches = viso.getNumberOfMatches();
                double num_inliers = viso.getNumberOfInliers();
                std::vector<Matcher::p_match> p_matches = viso.getMatches();

                cout << ", Matches: " << num_matches;
                cout << ", Inliers: " << 100.0*num_inliers/num_matches << " %" << ", Current pose: " << endl;
                cout << pose << endl << endl;

                // draw the matches on the left current/previous image and right current/previous image
                // reduce the size of image to half size

                if(i > 0){

                    // this is the oppisite
                    cv::Mat display_img(height, width, CV_8UC1);
                    cv::Mat half_prev_left_img(half_height, half_width, CV_8UC1);
                    cv::Mat half_prev_right_img(half_height, half_width, CV_8UC1);
                    cv::Mat half_left_img(half_height, half_width, CV_8UC1);
                    cv::Mat half_right_img(half_height, half_width, CV_8UC1);
                    cv::resize(prev_left_img, half_prev_left_img,cv::Size(half_width, half_height), 0, 0, cv::INTER_LINEAR);
                    cv::resize(prev_right_img, half_prev_right_img,cv::Size(half_width, half_height), 0, 0, cv::INTER_LINEAR);
                    cv::resize(left_img, half_left_img,cv::Size(half_width, half_height), 0, 0, cv::INTER_LINEAR);
                    cv::resize(right_img, half_right_img,cv::Size(half_width, half_height), 0, 0, cv::INTER_LINEAR);

                    // previous left image
                    half_prev_left_img.copyTo(display_img(cv::Rect(0,0,half_width, half_height)));
                    // previous right image
                    half_prev_right_img.copyTo(display_img(cv::Rect(half_width, 0, half_width, half_height)));
                    // current left image
                    half_left_img.copyTo(display_img(cv::Rect(0, half_height, half_width, half_height))) ;
                    // current right image
                    half_right_img.copyTo(display_img(cv::Rect(half_width, half_height, half_width, half_height))) ;

                    cv::cvtColor(display_img, display_img, CV_GRAY2RGB);
                    // draw features correspondance
                    for(int j = 0, match_size = p_matches.size(); j < match_size;++j){
                        cv::Point prev_left_pt(p_matches[j].u1p/2, p_matches[j].v1p/2);
                        cv::Point prev_right_pt(p_matches[j].u1p/2+half_width, p_matches[j].v1p/2);
                        cv::Point cur_left_pt(p_matches[j].u1p/2, p_matches[j].v1p/2+half_height);
                        cv::Point cur_right_pt(p_matches[j].u1p/2+half_width, p_matches[j].v1p/2+half_height);

                        // draw the inliers
                        cv::circle(display_img, prev_left_pt, 2, cv::Scalar(0,255,0), 1);
                        cv::circle(display_img, prev_right_pt, 2,cv::Scalar(0,255,0), 1);
                        cv::circle(display_img, cur_left_pt, 2,cv::Scalar(0,255,0), 1);
                        cv::circle(display_img, cur_right_pt, 2,cv::Scalar(0,255,0), 1);

                        cv::line(display_img, prev_left_pt, cur_left_pt, cv::Scalar(0,0,255), 1);
                        cv::line(display_img, prev_right_pt, cur_right_pt, cv::Scalar(0,0,255), 1);
                    }

                    cv::imshow("Tracking_Res", display_img);
                    cv::waitKey(1);
                }

            } else {
                cout << " ... failed!" << endl;
            }

            /*------------------Pangolin Display-------------------*/
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            d_cam.Activate(s_cam);
            glClearColor(0.12f, 0.12f, 0.12f, 0.12f);

            pango.DrawGrids(p_G_C0, 500);
            FLOAT trans[3];
            pose.getData(trans, 0, 3, 2, 3);
//            cout << "Trans is: " << trans[0] << " " << trans[1] << " " << trans[2] << endl;
            Eigen::Vector3d p_G_Ci(trans[0], trans[1], trans[2]);
            trajectory.push_back(p_G_Ci);
            pango.DrawTrajectory(trajectory, Eigen::Vector3f(1,0,1));
//            pango.DrawTrajectory(Hugo.mTrajectory, Eigen::Vector3f(0,1,1));

            pangolin::FinishFrame();
            /*-----------------------------------------------------*/

            // release uint8_t buffers
            free(left_img_data);
            free(right_img_data);

            // catch image read errors here
        } catch (...) {
            cerr << "ERROR: Couldn't read input files!" << endl;
            return 1;
        }
    }

    // output
    cout << "Demo complete! Exiting ..." << endl;

    // exit
    return 0;
}

